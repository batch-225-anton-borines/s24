console.log("Hello, World!");

/*
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
*/

	let getCube = Math.pow(2,3);

/*
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/

	console.log(`The cube of 2 is ${getCube}.`);

/*
5. Create a variable address with a value of an array containing details of an address.
*/

	let address = [258,'Washington Ave. NW', 'California', 90011]

/*
6. Destructure the array and print out a message with the full address using Template Literals.
*/

	let [house, streetAddress, state, zipCode] = address;

	console.log(`I live at ${house} ${streetAddress}, ${state} ${zipCode}.`)

/*
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
*/

	let animal = { 
		name: 'Lolong',
		type: 'saltwater crocodile',
		weight: '1075 kgs',
		length: '20 ft',
		width: '3 in'
		}

/*

8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	let {name, type, weight, length, width} = animal;

	console.log(`${name} was a ${type}. He weighed ${weight} with a measurement of ${length} ${width}.`);

/*
9. Create an array of numbers.
*/

	let arr = [1, 2, 3, 4, 5, 6]

/*
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

	arr.forEach((i) => {
		console.log(i);
	})

/*

11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/


	let total = arr.reduce((sum,i) => {
		return sum + i;
	})

	console.log(total);

/*
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);