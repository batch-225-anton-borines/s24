console.log("Hello, World!");

// [SECTION] Exponent Operator

// ES5 Version
const firstNum = 8 ** 2;
console.log(firstNum);

// ES6 Update
const secondNum = Math.pow(8,2);
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write strings without using the concatenation operator
	- Greatly helps with code readability
*/

let name = "John";

// pre-Template Literals string
// ES5 Version

let message = "Hello, " + name + "! Welcome to programming!";

console.log("Message without template literals: " + message);

// String using template literals
// Using backticks (``);
// If it's inside backticks, use the syntax ${element} to insert a variable

message = `Hello, ${name}! Welcome to programming!`;

console.log(`Message with template literals: ${message}`);

// Multi-line using Template Literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}.	
`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1769;
console.log(`The interest on your savings account is ${principal * interestRate}.`);
console.log(`The interest on your savings account is ${Math.round(principal * interestRate)}.`);

// [SECTION] Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps in code readability
	
	Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName
*/


const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring --> ES5
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// Array Destructuring --> ES6

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// [SECTION] Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	
	Syntax:
		let/const {propertyName, propertyName, propertyName} = object;
*/
const person = {
    givenName: "Jane",
    middleName: "Dela",
    familyName: "Cruz"
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.middleName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.middleName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring
const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(middleName);
console.log(familyName);

console.log(`Hello ${givenName} ${middleName} ${familyName}! It's good to see you again.`);

function getFullName ({ givenName, middle, familyName}) {
    console.log(`${ givenName } ${ middleName } ${ familyName }`);
}

getFullName(person);

// [SECTION] Arrow Functions
/*
	- Compact alternate syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- "DRY" (Don't Repeat Yourself) principle where there's no longer any need to create a function and think of a name for functions that will only be used in certain code snippets

	Syntax:
		const variableName = () => {
			statement;
		}
*/

const hello = () => {
	console.log("Hello, World!");
}

hello();

// Pre-Arrow Function and Template Literals --> ES5
/*
	- Syntax
		function functionName(parameterA, parameterB, parameterC) {
			console.log();
		}
*/

function printFullName (firstName, middleInitial, lastName) {
    console.log(firstName + ' ' + middleInitial + '. ' + lastName);
}

printFullName("John", "D", "Smith");

// Arrow Function --> ES6
/*
	- Syntax
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log();
		}
*/

const printName = (first, middle, last) => {
    console.log(`${first} ${middle}. ${last}`);
}

printName("Matthew", "K", "George");

// Arrow Functions with loops
// Pre-Arrow Function

const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student.`);
})

// Arrow Function
// The function is only used in the "forEach" method to print out a text with the student's names
students.forEach((student) => {
	console.log(`${student} is a student.`);
})

// [SECTION] Implicit Return Statement
/*
	- There are instances when you ommit the "return" statement
	- This works because even without the "return" JavaScript immplicity adds it for the result of the function

	Syntax:
	// Pre-Arrow Function
		const add = (x, y) => {
			return x + y;
		}
*/

const add = (x, y) =>  x + y;

let total = add(1,2);
console.log(total);

// [SECTION] Default Function Argument Value
/*
	- Provides a default argument value if none is provided when the function is invoked.

	Syntax:
		const functionName = (defaultVar = defaultElement) => {
			statement;
		}
*/

const greet = (name = 'username') => {
	return `Good Morning, ${name}!`
}

console.log(greet("Anton"));


// [SECTION] Class-Based Object Blueprints
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/

// Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class.
	- The "this" keyword refers to the properties of an object created/initialized from the class
	- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
	- Syntax
		class className {
			constructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

/*
	ES5 
	function Pokemon(name,level) {

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;
		}
*/


class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object
/*
	- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
	- No arguments provided will create an object without any values assigned to it's properties
	- let/const variableName = new ClassName();
*/
// let myCar = new Car();

/*
	- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
	- It does not mean that it's properties cannot be changed/immutable
*/
const myCar = new Car();

console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// Creating/instantiating a new object from the car class with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);